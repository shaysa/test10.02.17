import { Component, OnInit } from '@angular/core';
import { ProductsService } from './products.service';

@Component({
  selector: 'jce-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
products;

//columns=['pid','pname','cost','categoryId'];

  columns=[1,2,3,4,5];
  columns2=[1,2,3];

 deleteProduct(product){
  this._productService.deleteProduct(product);
}


 constructor(private _productService:ProductsService) { }

  ngOnInit() {
    this._productService.getProducts().subscribe(productData => {this.products=productData});
  }

}
