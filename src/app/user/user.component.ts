
import { Component, OnInit, EventEmitter , Output } from '@angular/core';
import {User} from './user';

@Component({
  selector: 'jce-user',

  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css'],
  inputs:['user']  // הגדרנו תכונה שנקראת user  i added the user2 to save user whan delete 
                  // אפשר להוסיף עוד תכונה באמצעות פסיק 
})
export class UserComponent implements OnInit {

user:User; // השמה, יוזר הוא מסוג יוזר
  @Output('name') tempName: string;     //הכנסה של ערכים למשתנה נוסף מסוג סטרינג 
  @Output('email') tempEmail: string;   //הכנסה של ערכים למשתנה נוסף מסוג סטרינג 

@Output() deleteEvent = new EventEmitter<User>();  //זה משנה ביוסארס וצריך להוסיף משו כזה לאדייט של יוזר
@Output() aditEvent = new EventEmitter<User>();

isEdit: Boolean =false;  //פה נקבע איך יתחיל הטופס
editButtonText = 'Edit';


  constructor() { }

  sendDelete(){

this.deleteEvent.emit(this.user) ;

  }


  // cancelEdit(){
  // this.isEdit = !this.isEdit;
  //  this.isEdit ? 


  // }
  cancelEdit(){                          //פונקציה ששומרת את הערכים למקרה שנרצה לבטל את השינוי 
    this.isEdit = false;
    this.user.email = this.tempEmail;
    this.user.name = this.tempName;
    this.editButtonText = 'Edit'; 
  }




  toggleEdit(){
     this.tempEmail = this.user.email;
     this.tempName = this.user.name;
  this.isEdit = !this.isEdit; // שיוני של עצמו 
  this.isEdit ? this.editButtonText = 'save' : this.editButtonText = 'Edit'; //ככה נראה לולאת אייפ כאשר אחרי ה ? הראשון הוא אמת השני שקר
  if(!this.isEdit){
    this.aditEvent.emit(this.user);

  }

  }

  ngOnInit() {
     
  }
}