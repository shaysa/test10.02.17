import { Component, OnInit , Output, EventEmitter} from '@angular/core';
import {Product} from './product';
@Component({
  selector: 'jce-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css'],
  inputs:["product" ,"column"]
})
export class ProductComponent implements OnInit {

product:Product;

 @Output() deleteEvent = new EventEmitter<Product>();
 

  sendDelete(){
  this.deleteEvent.emit(this.product);
}
  constructor() { }
    

  ngOnInit() {
  }

}
