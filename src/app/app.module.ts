import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import {RouterModule , Routes} from '@angular/router'; //יבוא ה routes options 

import {AngularFireModule} from 'angularfire2';

import { AppComponent } from './app.component';
import { UsersComponent } from './users/users.component';
import {UsersService} from './users/users.service';
import {ProductsService} from './products/products.service';



import { UserComponent } from './user/user.component';
import { SpinnerComponent } from './shared/spinner/spinner.component';
import {PostsComponent} from './posts/posts.component';
import {PageNOTFoundComponent} from './page-notfound/page-notfound.component';
import { UserFormComponent } from './user-form/user-form.component';
import { LeadsComponent } from './leads/leads.component';
import { ProductComponent } from './product/product.component';
import { ProductsComponent } from './products/products.component';
import { InvoiceFormComponent } from './invoice-form/invoice-form.component';
import { InvoiceComponent } from './invoice/invoice.component';




// we added this for angularfirebase
export const firebaseConfig = {

      apiKey: "AIzaSyCNBHkztl8TIKgUItVXZA5_7pRPLHM-DMk",
    authDomain: "shaiproject-fa0c5.firebaseapp.com",
    databaseURL: "https://shaiproject-fa0c5.firebaseio.com",
    storageBucket: "shaiproject-fa0c5.appspot.com",
    messagingSenderId: "252287982004"

   }



const appRoutes:Routes =[
   {path:'users', component:UsersComponent} , //כאשר הזיהוי הוא users  תעשה 
    {path:'posts', component:PostsComponent} ,
     {path:'product', component:ProductComponent} ,
      {path:'products', component:ProductsComponent} ,
     //כאשר לא קיים פאס הדיפולט הוא יוסרס 
     {path:'leads', component:LeadsComponent},
     {path:'', component:InvoiceFormComponent} ,
    {path:'**', component:PageNOTFoundComponent} ,
    {path:'invoice', component:InvoiceComponent} ,
    {path:'invoiceForm', component:InvoiceFormComponent} , // פאס שלא קיים יחזיר לא קיים, שני כוכביות תופס כל פאס אחר שלא הוגדר אצלנו 
]

@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    UserComponent,
    SpinnerComponent,
    PostsComponent,
    PageNOTFoundComponent,
    UserFormComponent,
    LeadsComponent,
    ProductComponent,
    ProductsComponent,
    InvoiceFormComponent,
    InvoiceComponent,
    
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule,
    RouterModule.forRoot(appRoutes),
   //we add in for angularfirebase
   AngularFireModule.initializeApp(firebaseConfig)

  ],
  providers: [UsersService,ProductsService], //לא לשכוח להוסיף את זה בשביל שיעבוד
  bootstrap: [AppComponent]
})
export class AppModule { }