import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'jce-leads',
  templateUrl: './leads.component.html',
  // שינינו פה את הסטיל במקום סטייל יו אר אל שינינו את הסטייל כללי לעיצוב במעבר על השורות 
   styles: [`
    .leads li { cursor: default; }
    .leads li:hover { background: #ecf0f1; } 
   `]
})
export class LeadsComponent implements OnInit {

  leads =[
     {id:'001', sorce:'web'},
     {id:'002', sorce:'phone'},
     {id:'003', sorce:'freind'}

  ]

  constructor() { }

  ngOnInit() {
  }

}
